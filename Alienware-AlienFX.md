# Alienware AlienFX

[[_TOC_]]

Alienware laptops provide an RGB control system called AlienFX.  This project
has some support for this RGB system:

<https://github.com/bchretien/AlienFxLite>

There is a newer controller that is described in some detail
[here](https://github.com/trackmastersteve/alienfx/issues/41#issuecomment-465388185).

## Old controller
Looking through the code in AlienFxLite, it looks like the AlienFX system is a
USB controller that has a vendor ID of `187C` and a product ID of `0512`.  It
uses USB control transfers with the following parameters:

``` C++
#define SEND_REQUEST_TYPE 0x21
#define SEND_REQUEST      0x09
#define SEND_VALUE        0x202
#define SEND_INDEX        0x00

#define READ_REQUEST_TYPE 0xA1
#define READ_REQUEST      0x01
#define READ_VALUE        0x101
#define READ_INDEX        0x0
```

The AlienFX controller uses 4 bits per color channel.

### Commands

| Command Value | Command Description  |
| :-----------: | :------------------- |
| 0x00          | End Storage Block    |
| 0x01          | Set Morph Color      |
| 0x02          | Set Blink Color      |
| 0x03          | Set Color            |
| 0x04          | Loop Block End       |
| 0x05          | Transmit and Execute |

## New controller (AW-ELC)

The new controller is a USB HID controller that has a vendor ID of `187C` and a
product ID of `0550`. The protocol is different from the older controller.

The controller has a single USB interface and configuration. The USB
configuration identifies the device as an HID class device. The HID device
descriptor indicates that the device expects and returns reports 33 bytes in
length, where each byte can be anything between 0 and 255 in value.

Most of the known protocol information came from [this issue report
comment](https://github.com/trackmastersteve/alienfx/issues/41#issuecomment-465388185)
in the alienfx project, and reinforced through experimentation and analyzing
the Alienware Command Center application.

Conceptually, the RGB device is partitioned into zones. Each zone has a mode or
effect, color, brightness, duration, and tempo associated with it, independent
of all other zones.

One important observation about at least some of these controllers is that
**the RGB hardware will crash if bombarded with too many USB requests in a
short window of time, with only a full power-cycle being able to bring it back
up**. The solution to this is to make sure to give the controller some time to
process incoming commands. Also, some combinations of arguments apparently
cause the controller to crash as well. It appears that the amount of time that
one needs to wait between commands might have to do with how much work the RGB
controller might be doing for each task. Something that seems to work is to
send all the reports necessary to update an single animation, then let the
controller rest for a second. Other commands may/may not need delays between
them.

### General command structure

The USB HID report is 33 bytes long, both for outgoing and incoming reports. It
appears that every outgoing report causes the controller to generate an
incoming status report, indicating whether the requested operation succeeded or
failed. Success is usually denoted by padding everything after command with
0x00. On an error, the controller appears to returns the full report it
received. Some commands, like 0x21, appear to return the result at some
position in the response, which appears to vary based on the subcommand.

#### Outgoing command format
| Preamble | Command | Arguments/sub-commands ... | Padding               |
| :------: | :------ | :------------------------- | :-------------------- |
| 0x03     | 1 byte  | Command dependent          | 0x00 to fill the rest |

#### Incoming command result (success)
| Preamble | Command | Padding               |
| :------: | :------ | :-------------------- |
| 0x03     | 1 byte  | 0x00 to fill the rest |

#### Incoming command result (failure)
This should be the same as the report that caused the error.

| Preamble | Command | Arguments/sub-commands ... | Padding               |
| :------: | :------ | :------------------------- | :-------------------- |
| 0x03     | 1 byte  | Command dependent          | 0x00 to fill the rest |

Just to stress the point, some commands, like 0x21, report the success or error
by modifying a single byte in the response.

### Commands

| Command | Description                      |
| :-----: | :------------------------------- |
| 0x20    | Request status/info reports      |
| 0x21    | Configure user animations        |
| 0x22    | Configure power animations[^1]   |
| 0x23    | Select zones to apply actions to |
| 0x24    | Add action                       |
| 0x25    | Set event[^1]                    |
| 0x26    | Set dim                          |
| 0x27    | Set (macro?) color[^2]           |
| 0x28    | Reset[^1]                        |
| 0xFF    | Erase SPI flash[^1]              |

[^1]: These do not appear to be used by the Alienware Control Center
  application on Windows for the Dell G5 SE 5505 keyboard, which was used to
  test the functionality for the initial version of this document, so it is not
  clear if these do what their names suggest.

[^2]: The name is slightly misleading, as this command is some kind of a
  temporary set color. If an animation is active, the animation will very
  quickly override this command.

### Command 0x20, Request status/info reports

This command informs the controller to set up a response for a subsequent USB
HID GET REPORT request. Subcommands are a single byte. The incoming reports are
33 bytes in size. The response format includes the preamble, command, and
subcommand of the last status command, followed by the subcommand specific
response.

#### Command format
| Preamble | Command | Subcommand |
| :------: | :-----: | :--------- |
| 0x03     | 0x20    | 1 byte     |

#### Response format
| Preamble | Command | Subcommand | Data                    |
| :------: | :-----: | :--------- | :---------------------- |
| 0x03     | 0x20    | 1 byte     | Subcommand specific ... |

#### Subcommands
| Subcommand | Description              |
| :--------: | :----------------------- |
| 0x00       | Get firmware version     |
| 0x01       | Get status               |
| 0x02       | Get ELC firmware config  |
| 0x03       | Get animation count      |
| 0x04       | Get animation by ID      |
| 0x05       | Get series               |
| 0x06       | Get action???            |
| 0x07       | Caldera status???        |

It appears that unimplemented or unsupported subcommands return 0xFF outside of
the preamble, command, and subcommand. Interestingly enough, on a Dell G5 SE
5505, subcommands 0x07 and subcommand 0x08 return all 0x00.

There is also a possibility that the exact format of the following subcommands
might vary slightly depending on the platform. There is evidence in the
Alienware Control Center implementation for the 0x03 subcommand that the max ID
is a single byte, yet the output of the report on a Dell G5 SE 5505 looks like
this maximum ID is a 2 byte value.

#### Subcommand 0x00, Get firmware version
This report contains the firmware version in the form of
`{MAJOR}.{MINOR}.{REVISION}`.

| Preamble | Command | Subcommand | Major  | Minor  | Revision |
| :------: | :-----: | :--------: | :----- | :----- | :------- |
| 0x03     | 0x20    | 0x00       | 1 byte | 1 byte | 1 byte   |

On a Dell G5 SE 5505 as of the time of this writing, this returns 1.0.12, which
coincides with the version of the firmware package in the Alienware Command
Center software folders.

#### Subcommand 0x01, Get status
It is not clear what all possible status codes are, but it appears that 0x00
indicates no problems, and 0x01 indicates an error.

| Preamble | Command | Subcommand | Status |
| :------: | :-----: | :--------: | :----- |
| 0x03     | 0x20    | 0x01       | 1 byte |

| Error | Description |
| :---: | :---------- |
| 0x00  | OK          |
| 0x01  | Error       |

#### Subcommand 0x02, Get ELC firmware configuration
| Preamble | Command | Subcommand | Platform ID | Number of LEDs/zones |
| :------: | :-----: | :--------: | :---------- | :------------------- |
| 0x03     | 0x20    | 0x02       | 2 bytes     | 1 byte               |

Hopefully the platform ID is unique per kind of system, so that it can be used
to disambiguate different keyboard/zone configurations.

On a Dell G5 SE 5505 as of the time of this writing, this returns 0x0c01 for
the platform ID, and 16 zones (which is strange because this particular board
only has 4 visible zones).

#### Subcommand 0x03, Get animation count
| Preamble | Command | Subcommand | Number of animations | Index of current highest animation |
| :------: | :-----: | :--------: | :------------------- | :--------------------------------- |
| 0x03     | 0x20    | 0x03       | 2 bytes              | 2 bytes                            |

On a Dell G5 SE 5505 as of the time of this writing with defaults set by the
Alienware Command Center application, this returns 2 for the number of
animations (The startup animation at ID 0x0008 and 0x0061 for the default
keyboard animation/color), and 97 (or 0x0061) for the animation currently in
the highest index.

#### Subcommand 0x04, Get animation by ID
| Preamble | Command | Subcommand | ID of current animation | Loop   | Max number of animations/max index |
| :------: | :-----: | :--------: | :---------------------- | :----- | :--------------------------------- |
| 0x03     | 0x20    | 0x04       | 2 bytes                 | 1 byte | 1 byte                             |

This is unimplemented on a Dell G5 SE 5505.

#### Subcommand 0x05, Get series
| Preamble | Command | Subcommand | Loop   | Page?   | LED data?       |
| :------: | :-----: | :--------: | :----- | :------ | :-------------- |
| 0x03     | 0x20    | 0x05       | 1 byte | 4 bytes | 1 byte per LED? |

This is unimplemented on a Dell G5 SE 5505.

#### Subcommand 0x06, Get action???
| Preamble | Command | Subcommand | Unknown |
| :------: | :-----: | :--------: | :------ |
| 0x03     | 0x20    | 0x06       |         |

This is unimplemented on a Dell G5 SE 5505.

#### Subcommand 0x07, Get Caldera status???
Based on a cursory scan online, Caldera seems to be the codename for some Dell
graphic amplifiers, so this might be to query the status of such?

| Preamble | Command | Subcommand | Unknown |
| :------: | :-----: | :--------: | :------ |
| 0x03     | 0x20    | 0x07       |         |

This appears to be implemented on a Dell G5 SE 5505, and it only returns 0x00,
possibly indicating that everything is OK. It is possible that an error is
reported by setting one of the response bytes to something not 0x00.

### Command 0x21, Configure user animations

This command informs the controller to manage user animations, including
starting a new one. This is how colors and modes are changed. Subcommands are
two bytes in size, followed by a two byte animation ID, and followed by
supposedly a two byte duration (in milliseconds). In all the traces for the
Dell G5 SE 5505, the duration was always 0 except for when setting the startup
animation with subcommand 0x0007, which is about as long as the animation lasts
on startup, so maybe the value is related?

The return of each of the subcommands differ in where the status byte is
located.

#### Command format
| Preamble | Command | Subcommand | Animation ID | Duration     |
| :------: | :-----: | :--------- | :----------- | :----------- |
| 0x03     | 0x21    | 2 bytes    | 2 bytes      | 2 bytes (ms) |

#### Subcommands
| Subcommand | Description               | Status byte              |
| :--------: | :------------------------ | :----------------------- |
| 0x0001     | Start a new animation     | ? (might always succeed) |
| 0x0002     | Finish and save animation | 7th byte                 |
| 0x0003     | Finish and play animation | 5th byte?                |
| 0x0004     | Remove an animation       | ? (might always succeed) |
| 0x0005     | Play an animation         | 7th byte                 |
| 0x0006     | Set Default animation     | ?                        |
| 0x0007     | Set Startup animation     | ?                        |

#### Animation IDs
| Animation ID | Description                |
| :----------: | :------------------------- |
| 0x0008[^3]   | Default bootup animation   |
| 0x0061[^3]   | Default keyboard animation |
| 0xFFFF       | Ephemeral animation?       |

0xFFFF appears to be a special animation index (or it could be that for finish
and play nothing is saved?), but in all traces 0xFFFF (and 0xFF sometimes for
some reason???) are used when starting and ending a sequence with finish and
play. It appears that the controller does not save this animation in flash, but
stores it somewhere on the controller that is wiped on power loss (a reboot
does not appear to wipe it).

[^3]: These animation IDs appear to be a Dell convention, as it is possible to
  select other animations as the default bootup and keyboard animation. The
  Alienware Command Center application sets up these animations at these IDs.

### Command 0x22, Configure power animations

The subcommand and argument order is the same as for command 0x21. These are
not used on a Dell G5 SE 5505, so this functionality has not been verified.

#### Command format
| Preamble | Command | Subcommand | Animation ID | Duration?    |
| :------: | :-----: | :--------- | :----------- | :----------- |
| 0x03     | 0x22    | 2 bytes    | 2 bytes      | 2 bytes (ms) |

#### Animation IDs
| Animation ID | Description      |
| :----------: | :--------------- |
| 0x005B       | AC Sleep         |
| 0x005C       | AC on (charged)  |
| 0x005D       | AC on (charging) |
| 0x005E       | Battery sleep    |
| 0x005F       | Battery on       |
| 0x0060       | Battery critical |

### Command 0x23, Select zones to apply actions to

This command selects which zones to apply actions to. The command code is
followed by a one byte boolean value indicating whether the animation should
loop for the zone, followed by two bytes indicating the number of zones to
select, and followed by one byte index per zone to modify.

#### Command format
| Preamble | Command | Loop   | Number of zones | Zones            |
| :------: | :-----: | :----- | :-------------- | :--------------- |
| 0x03     | 0x23    | 1 byte | 2 bytes         | 1 bytes per zone |

### Command 0x24, Add action

This command informs the controller to change/set the mode of the current
animation of the zones currently selected. It appears that (at least some)
modes can be concatenated to go through a sequence.

Duration and tempo are given in milliseconds. The Alienware Control Center
application uses 2500ms as the longest duration value, and 1000ms as the
shortest. From experimentation it looks like the controller will accept any
value between those two. It may accept values outside those, but this was not
tested. For tempo, the longest value ACC uses is 250ms, and the shortest is
64ms, with the controller accepting any value between those (did not test
values outside the range).

#### Command format
| Preamble | Command | Subcommand | Duration     | Tempo        | RGB color              | ...[^4] |
| :------: | :-----: | :--------- | :----------- | :----------- | :--------------------- | :------ |
| 0x03     | 0x24    | 1 byte     | 2 bytes (ms) | 2 bytes (ms) | 3 bytes, one per color | ...[^4] |

[^4]: Some effects, such as morph, breathe, spectrum, and rainbow wave require
  a second subcommand. The second subcommand goes right after the RGB colors of
  the first one, and has the same layout as the arguments of the first
  subcommand.

#### Subcommands
| Subcommand | Description |
| :--------: | :---------- |
| 0x00       | Set color   |
| 0x01       | Set pulse   |
| 0x02       | Set Morph   |

It appears that breathe, spectrum, etc. effects are merely instances of Morph
with different configuration values and modifying zones.

The basic sequence for configuring an animation is:
 1. Start a new animation
 2. Set the zones to be modified
 3. Add an action
 4. Repeat 2. and 3. as necessary to modify more zones
 5. Finish and play animation (or finish and save to an unused animation index)

### Command 0x25, Set event

Unknown, did not observe in captures.

### Command 0x26, Set dim

This command controls the brightness of the specified zones. Brightness is
handled independently of color. The dim value is a number between 0 (for not
dim at all, or full brightness), and 100 (or 0x64 for full dim, or off).

#### Command format

| Preamble | Command | Dim    | Number of zones | Zones            |
| :------: | :-----: | :----- | :-------------- | :--------------- |
| 0x03     | 0x26    | 1 byte | 2 bytes         | 1 bytes per zone |

### Command 0x27, Set color

This command sets the color of the specified zones temporarily. A
currently-active animation will override this command, which will look like a
brief flash before returning to the animation color.

It appears possible to set up a custom animation with no looping and a very
short duration to enable this command to work. Some controllers (such as on the
Dell G5 SE 5505) turn off the RGB lights after a timeout (specified in UEFI or
some other menu), which also appears to wipe the last color set by this
command. On wake up, it appears the last active animation runs (which can be the
short duration, no looping animation to enable for the continued use of this
command moving forward).

#### Command format
| Preamble | Command | RGB color              | Number of zones | Zone index      |
| :------: | :-----: | :--------------------- | :-------------- |:--------------- |
| 0x03     | 0x27    | 3 bytes, one per color | 2 bytes         | 1 byte per zone |

### Command 0x28, Reset

This appears to clear the controller's current animation from its memory, so it
might also be another way to stop playing animations for use with command 0x27.
It is not clear if this does anything else to the controller.

Target may be 0x00 for some microcontroller on the controller, and 0x01 may be
for all elements of the controller.

#### Possible command format
| Preamble | Command | Target? |
| :------: | :-----: | :------ |
| 0x03     | 0x28    | 1 byte  |

### Command 0xFF, Erase SPI flash

This appears to clear the controllers flash storage. This removes all stored
animations, including defaults loaded by the Alienware Command Control
software.

#### Possible command format
| Preamble | Command |
| :------: | :-----: |
| 0x03     | 0xFF    |

### Examples

These examples assume there are 16 zones in the device. The number of zones per
actual device varies (the Dell G5 SE 5505 keyboard has 4 zones, for example).

#### Turn off all lights (Dim command)
```
03 26 64 00 10 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0F 00 00 00 00 00 00 00 00 00 00 00 00 00
```

#### Turn on all lights (Dim command)
```
03 26 00 00 10 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0F 00 00 00 00 00 00 00 00 00 00 00 00 00
```

#### Turn off 2 zones, zone 0 and zone 3 (Dim command)
```
03 26 64 00 02 00 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
```

#### Change color of all zones to blue
```
03 21 00 01 FF FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 23 01 00 10 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 00 00 00 00 00 00 00 00 00 00 00 00
03 24 00 07 D0 00 FA 00 00 FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 21 00 03 FF FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
```

#### Change color of half the zones to white, and half to red
```
03 21 00 01 FF FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 23 01 00 08 00 01 02 03 04 05 06 07 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 24 00 07 D0 00 FA FF FF FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 23 00 00 08 08 09 0A 0B 0C 0D 0E 0F 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 24 00 07 D0 00 FA FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 21 00 03 FF FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
```

#### Morph between white and green
```
03 21 00 01 FF FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 23 01 00 10 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 00 00 00 00 00 00 00 00 00 00 00 00
03 24 02 07 D0 00 FA FF FF FF 02 07 D0 00 FA 00 FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 21 00 03 FF FF 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
```

### Troubleshooting

#### The controller is no longer responsive, or there are kernel messages indicating it cannot communicate with the controller

The controller crashed for some reason (too many messages in short order, or a
bad argument, or a combination of different factors, it really does not provide
a lot of useful insight into what caused it to die). It needs to be power
cycled, and rebooting may not be enough. Power off the computer completely, and
turn on the computer again. If there is still a default startup animation
enabled (and dim is set to 0, or lights are enabled), the startup animation
should show on power on.

#### I was tinkering with the erase SPI command, and I've lost my default animations!

Reinstall ACC on Windows, this will reset the controller to the Dell defaults.
If this is not an option, doing the following should set up the defaults (at
least for a Dell G5 Se 5505):
 1. Remove animation `0x0008`
 2. Remove animation `0x0061`
 3. Start a new animation `0x0008`
 4. Set zones with looping enabled for all zones on your controller (4 for the
	Dell G5 SE 5505)
 5. First add action:
	1. duration1 = `0x0282`, tempo1 = `0x000F`, color1 = `0xFF0000`
	2. duration2 = `0x0282`, tempo2 = `0x000F`, color2 = `0xFFA500`
	3. duration3 = `0x0282`, tempo3 = `0x000F`, color3 = `0xFFFF00`
 6. Second add action:
	1. duration1 = `0x0282`, tempo1 = `0x000F`, color1 = `0x008000`
	2. duration2 = `0x0282`, tempo2 = `0x000F`, color2 = `0x00BFFF`
	3. duration3 = `0x0282`, tempo3 = `0x000F`, color3 = `0x0000FF`
 7. Third add action:
	1. duration1 = `0x0282`, tempo1 = `0x000F`, color1 = `0x800080`
 8. Finish and save animation to `0x0008`
 9. Start a new animation `0x0061`
 10. Set zones with looping enabled for all zones on your controller (4 for the
	 Dell G5 SE 5505)
 11. Add action:
	1. duration = 1000 (`0x03E8`), tempo = 100 (`0x0064`), color = `0x00F0F0`
 12. Finish and save animation to `0x0061`
 13. Set default animation to `0x0061`
 13. Set startup animation to `0x0008` with duration 6000 (`0x1770`)

This is what the steps above look like as outgoing USB reports:
```
03 21 00 04 00 08 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 21 00 04 00 61 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 21 00 01 00 08 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 23 01 00 10 00 01 02 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 24 02 02 82 00 0F FF 00 00 02 02 82 00 0F FF A5 00 02 02 82 00 0F FF FF 00 00 00 00 00 00 00 00
03 24 02 02 82 00 0F 00 80 00 02 02 82 00 0F 00 BF FF 02 02 82 00 0F 00 00 FF 00 00 00 00 00 00 00
03 24 02 02 82 00 0F 80 00 80 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 21 00 02 00 08 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 21 00 01 00 61 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 23 01 00 10 00 01 02 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 24 00 03 E8 00 64 00 F0 F0 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 21 00 02 00 61 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 21 00 06 00 61 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
03 21 00 07 00 08 17 70 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
```

# Footnotes

# Corsair K55 RGB Pro XT

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1B1C` | `1BA1` | Corsair K55 RGB PRO XT |

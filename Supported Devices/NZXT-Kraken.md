# NZXT Kraken

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1E71` | `170E` | NZXT Kraken X2 |
| `1E71` | `1715` | NZXT Kraken M2 |

# Logitech X56

 

## Connection Type
 USB

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0738` | `2221` | Logitech X56 Rhino Hotas Joystick |
| `0738` | `A221` | Logitech X56 Rhino Hotas Throttle |

# Corsair Lighting Node

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1B1C` | `0C1A` | Corsair Lighting Node Core |
| `1B1C` | `0C0B` | Corsair Lighting Node Pro |
| `1B1C` | `0C10` | Corsair Commander Pro |
| `1B1C` | `0C1E` | Corsair LS100 Lighting Kit |
| `1B1C` | `1D00` | Corsair 1000D Obsidian |
| `1B1C` | `1D04` | Corsair SPEC OMEGA RGB |
| `1B1C` | `0C23` | Corsair LT100 |

# Corsair Vengeance

 

## Connection Type
 SMBus

## Saving
 Controller saves automatically on every update

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `:` | `:` |  |

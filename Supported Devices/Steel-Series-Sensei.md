# Steel Series Sensei

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1038` | `1720` | SteelSeries Rival 310 |
| `1038` | `171e` | SteelSeries Rival 310 CS:GO Howl Edition |
| `1038` | `1736` | SteelSeries Rival 310 PUBG Edition |
| `1038` | `1832` | SteelSeries Sensei TEN |
| `1038` | `1834` | SteelSeries Sensei TEN CS:GO Neon Rider Edition |
| `1038` | `1722` | SteelSeries Sensei 310 |

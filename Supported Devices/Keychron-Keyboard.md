# Keychron Keyboard

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `05AC` | `024F` | Keychron Gaming Keyboard 1 |

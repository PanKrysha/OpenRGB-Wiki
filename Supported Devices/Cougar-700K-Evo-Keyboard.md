# Cougar 700K Evo Keyboard

 The Cougar 700K Evo controller implements all hardware modes
found in the OEM software but has not been able to include all
options for some modes. eg. Rainbow colour mode. Music mode was
deteremined to be a software driven effect which can be added with
the (OpenRGB Effects Engine plugin)[https://gitlab.com/OpenRGBDevelopers/OpenRGBEffectsPlugin].

## Connection Type
 USB

## Saving
 Saving is supported by this controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `060B` | `7010` | Cougar 700K EVO Gaming Keyboard |

# Dark Project Keyboard

 The Dark Project keyboard controller currently supports
the full size KD3B Version 2 (ANSI layout).

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `195D` | `2061` | Dark Project KD3B V2 |

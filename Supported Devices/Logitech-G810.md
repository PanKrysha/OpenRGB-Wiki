# Logitech G810

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `046D` | `C342` | Logitech G512 |
| `046D` | `C33C` | Logitech G512 RGB |
| `046D` | `C333` | Logitech G610 Orion |
| `046D` | `C338` | Logitech G610 Orion |
| `046D` | `C331` | Logitech G810 Orion Spectrum |
| `046D` | `C337` | Logitech G810 Orion Spectrum |

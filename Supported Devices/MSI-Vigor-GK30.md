# MSI Vigor GK30

 This device does only support 7 different colors

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0DB0` | `0B30` | MSI Vigor GK30 controller |

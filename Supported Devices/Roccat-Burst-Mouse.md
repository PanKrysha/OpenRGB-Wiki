# Roccat Burst Mouse

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1E7D` | `2DE6` | Roccat Burst Core |
| `1E7D` | `2DE1` | Roccat Burst Pro |

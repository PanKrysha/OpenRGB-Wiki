The Corsair K57 RGB keyboard uses a different protocol from the other Corsair keyboards.

All packets are 64 bytes.

The first byte of the packet seems to indicate direction.  0x80 for host -> device and 0x00 for device->host

# Enter Direct Mode

| Index | Value     |
| ----- | --------- |
| 0x00  | Direction |
| 0x01  | 0x01      |
| 0x02  | 0x03      |
| 0x03  | 0x00      |
| 0x04  | 0x02      |

# Start Direct Mode

| Index | Value     |
| ----- | --------- |
| 0x00  | Direction |
| 0x01  | 0x0D      |
| 0x02  | 0x00      |
| 0x03  | 0x01      |

# Exit Direct Mode

| Index | Value     |
| ----- | --------- |
| 0x00  | Direction |
| 0x01  | 0x01      |
| 0x02  | 0x03      |
| 0x03  | 0x00      |
| 0x04  | 0x01      |

# Direct Mode Start (0x06) - 57 LED Bytes

| Index | Value          |
| ----- | -------------- |
| 0x00  | Direction      |
| 0x01  | 0x06           |
| 0x02  | 0x00           |
| 0x03  | 0x9B           |
| 0x04  | 0x01           |
| 0x05  | 0x00           |
| 0x06  | 0x00           |
| 0x07  | First LED Byte |

# Direct Mode Data (0x07) - 61 LED Bytes

| Index | Value          |
| ----- | -------------- |
| 0x00  | Direction      |
| 0x01  | 0x07           |
| 0x02  | 0x00           |
| 0x03  | First LED byte |

# Matrix Map - 137 LEDs
```
0: Power/Wireless Indicator
1: Lock/Macro Indicator
2: N/A
3: N/A
4: Key: A
5: Key: B
6: Key: C
7: Key: D
8: Key: E
9: Key: F
10: Key: G
11: Key: H
12: Key: I
13: Key: J
14: Key: K
15: Key: L
16: Key: M
17: Key: N
18: Key: O
19: Key: P
20: Key: Q
21: Key: R
22: Key: S
23: Key: T
24: Key: U
25: Key: V
26: Key: W
27: Key: X
28: Key: Y
29: Key: Z
30: Key: 1
31: Key: 2
32: Key: 3
33: Key: 4
34: Key: 5
35: Key: 6
36: Key: 7
37: Key: 8
38: Key: 9
39: Key: 0
40: Key: Enter
41: Key: Escape
42: Key: Backspace
43: Key: Tab
44: Key: Space
45: Key: -
46: Key: =
47: Key: [
48: Key: ]
49: Key: \ (ANSI)
50: Unused
51: Key: ;
52: Key: '
53: Key: `
54: Key: ,
55: Key: .
56: Key: /
57: Key: Caps Lock
58: Key: F1
59: Key: F2
60: Key: F3
61: Key: F4
62: Key: F5
63: Key: F6
64: Key: F7
65: Key: F8
66: Key: F9
67: Key: F10
68: Key: F11
69: Key: F12
70: Key: Print Screen
71: Key: Scroll Lock
72: Key: Pause/Break
73: Key: Insert
74: Key: Home
75: Key: Page Up
76: Key: Delete
77: Key: End
78: Key: Page Down
79: Key: Right Arrow
80: Key: Left Arrow
81: Key: Down Arrow
82: Key: Up Arrow
83: Key: Num Lock
84: Key: Number Pad /
85: Key: Number Pad *
86: Key: Number Pad -
87: Key: Number Pad +
88: Key: Number Pad Enter
89: Key: Number Pad 1
90: Key: Number Pad 2
91: Key: Number Pad 3
92: Key: Number Pad 4
93: Key: Number Pad 5
94: Key: Number Pad 6
95: Key: Number Pad 7
96: Key: Number Pad 8
97: Key: Number Pad 9
98: Key: Number Pad 0
99: Key: Number Pad .
100: Unused
101: Key: Menu
102: Unused
103: Unused
104: Unused
105: Left Control
106: Left Shift
107: Left Alt
108: Left Windows
109: Right Control
110: Right Shift
111: Right Alt
112: Unused
113: Unused
114: Unused
115: Unused
116: Unused
117: Unused
118: Unused
119: Unused
120: Unused
121: Unused
122: Key: Right Fn
123: Unused
124: Unused
125: Unused
126: Unused
127: Unused
128: Unused
129: Unused
130: Unused
131: Key: G1
132: Key: G2
133: Key: G3
134: Key: G4
135: Key: G5
136: Key: G6
```